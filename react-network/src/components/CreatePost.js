import { useState } from "react"

function CreatePost({ addPost }) {

  const [postText, setPostText] = useState('')
  const [postPicture, setPostPicture] = useState('')

  const onPostTextChangeHandler = (event) => {
    setPostText(event.target.value)
  }

  const onPostPictureChangeHandler = (event) => {
    setPostPicture(event.target.value)
  }

  const createPostHandler = (event) => {
    addPost(postText, postPicture)
    setPostText('')
  }

  return (
    <div className='post'>
      <div className="createpost-row">
        <div>Photo de profil</div>
        <input type="text" value={postText} onChange={onPostTextChangeHandler} placeholder="Quoi de neuf aujourd'hui ?"></input>
      </div>
      <div className="createpost-row">
        <div>Photo : </div>
        <input type="text" value={postPicture} onChange={onPostPictureChangeHandler} placeholder="URL de la photo"></input>
      </div>
      {postText.trim() != '' && (
        <div className="createpost-row">
          <button className="btn" onClick={createPostHandler}>Publier</button>
        </div>
      )}
    </div>
  )
}

export default CreatePost;