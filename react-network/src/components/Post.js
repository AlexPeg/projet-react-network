import { useState } from 'react'
import * as moment from 'moment'

function Post({ postData, deletePost }) {

  const [nbLikes, setNbLikes] = useState(postData.likes)
  const [isLiked, setIsLiked] = useState(false)
  const [showMenu, setShowMenu] = useState(false)

  const likePost = () => {
    const increment = isLiked ? -1 : 1
    setNbLikes(nbLikes + increment)
    setIsLiked(!isLiked)
  }

  const toggleMenu = () => setShowMenu(!showMenu)

  return (
    <div className='post'>
      <div className='post-header'>
        <div className='post-header_left'>
          <img className='post-profilepic' src={postData.authorPicture} />
          <div>
            <div className='post-author'>{postData.author}</div>
            <div className='post-time'>{moment(postData.date).format('HH:mm')}</div>
          </div>
        </div>
        <div className='post-menu' onClick={toggleMenu}>
          ...
          <div className={`post-popup ${showMenu ? '' : 'hidden'}`} onClick={() => deletePost(postData.id)}>Supprimer</div>
        </div>
      </div>
      <div className="post-text">
        {postData.text}
      </div>
      <div className="post-picture">
        <img src={postData.postPicture} />
      </div>
      <div className="post-footer">
        <span className={`post-likes ${isLiked ? 'liked' : ''}`} onClick={likePost}>
          <span className="post-thumb">👍</span>
          <span>{nbLikes}</span>
        </span>
      </div>
    </div>
  )
}

export default Post;