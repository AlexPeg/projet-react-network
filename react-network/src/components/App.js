import { useState } from 'react'
import Header from './Header'
import Feed from './Feed'
import CreatePost from './CreatePost'

const initialPosts = [
  {
    id: 12,
    author: "Hugo Bordes",
    authorPicture: 'https://picsum.photos/seed/profile11/50/50',
    text: "Bonjour tout le monde !",
    postPicture: 'https://picsum.photos/seed/postpicture-12/500/300',
    date: new Date(Date.now() - 1 * 3600 * 1000),
    likes: 42,
  },
  {
    id: 55,
    author: "Antoine Morin",
    authorPicture: 'https://picsum.photos/seed/profile16/50/50',
    text: "Comment allez-vous ?",
    postPicture: 'https://picsum.photos/seed/postpicture-55/500/300',
    date: new Date(Date.now() - 3 * 3600 * 1000),
    likes: 35,
  },
  {
    id: 90,
    author: "Léa Dumont",
    authorPicture: 'https://picsum.photos/seed/profile17/50/50',
    text: "Vive React.JS",
    postPicture: 'https://picsum.photos/seed/postpicture-90/500/300',
    date: new Date(Date.now() - 2 * 3600 * 1000),
    likes: 17,
  },
]

const currentUser = {
  author: "Nouvel utilisateur",
  authorPicture: 'https://picsum.photos/seed/profile53/50/50',
}

function App() {

  const [posts, setPosts] = useState(initialPosts)

  const deletePost = (id) => {
    setPosts(posts.filter(p => p.id != id))
  }

  const addPost = (postText, postPicture) => {
    const newPost = {
      id: Math.floor(1000 * Math.random()), //entier aléatoire entre 0 et 1000
      author: currentUser.author,
      authorPicture: currentUser.authorPicture,
      text: postText,
      postPicture,
      date: new Date(),
      likes: 0
    }
    setPosts([...posts, newPost])
  }

  return (
    <div className="app">
      <Header />
      <div className='center500px'>
        <CreatePost addPost={addPost} />
        <Feed posts={posts} deletePost={deletePost} />
      </div>
    </div>
  );
}
export default App;