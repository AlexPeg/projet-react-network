import Post from './Post'
import emptyImage from '../images/empty.png'

function Feed({ posts, deletePost }) {

	//tri des posts du plus récent au plus ancien
	posts.sort((post1, post2) => post2.date.getTime() - post1.date.getTime())

	return posts.length > 0 ?
		(
			<>
				{posts.map(p => <Post key={p.id} postData={p} deletePost={deletePost} />)}
			</>
		) : (
			<div align="center">
				<img width="300" src={emptyImage} />
				<h3>Aucun post pour le moment</h3>
			</div>
		)

}

export default Feed